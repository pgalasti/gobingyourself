﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoBingYourself
{
    class StringParser
    {
        private String m_CurrentString = "";
        private char m_CurrentDelimiter = ' ';
        private List<String> m_Tokens = new List<String>();

        public List<String> Tokens { get { return m_Tokens; } }
        public char CurrentDelimiter { get { return m_CurrentDelimiter; } }
        public String CurrentString { get { return m_CurrentString; } }

        public StringParser()
        {

        }

        public StringParser(String stringToParse, char delimiter = ' ')
        {
            Parse(stringToParse, delimiter);
        }

        public List<String> Parse()
        {
            m_Tokens.Clear();
            String currentStringBuffer = "";
            foreach (char character in m_CurrentString)
            {
                if (character == m_CurrentDelimiter)
                {
                    if (currentStringBuffer.Length == 0)
                    {
                        m_Tokens.Add(m_CurrentDelimiter.ToString());
                        continue;
                    }

                    m_Tokens.Add(currentStringBuffer);
                    currentStringBuffer = "";
                    continue;
                }
                currentStringBuffer += character;
            }

            if (currentStringBuffer.Length > 0)
            {
                m_Tokens.Add(currentStringBuffer);
            }

            return Tokens;
        }

        public List<String> Parse(String stringToParse, char delimiter = ' ')
        {
            m_CurrentString = stringToParse;
            m_CurrentDelimiter = delimiter;

            return Parse();
        }
    }

    public class LineParser
    {
        private List<String> _Lines = new List<String>();
        private const char _LineDelimter = '\n';
        private String _CurrentText;

        public List<String> Lines { get { return _Lines; } }
        public char CurrentDelimiter { get { return _LineDelimter; } }
        public String CurrentText { get { return _CurrentText; } }

        public LineParser()
        {
            _CurrentText = "";


        }

        public List<String> Parse()
        {
            _Lines.Clear();

            String line = String.Empty;
            foreach (char character in _CurrentText)
            {
                if (character == '\n')
                {
                    _Lines.Add(line);
                    line = String.Empty;
                    continue;
                }

                line += character;
            }
            return Lines;
        }

        public List<String> Parse(String text)
        {
            _CurrentText = text;
            return Parse();
        }
    }
}
