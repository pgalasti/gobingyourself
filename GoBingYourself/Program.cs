﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GoBingYourself
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int queries = 30;

            if(args.Length > 1 )
            {
                YoureAFailureHarry();
                return;
            }
            else if (args.Length > 0)
            {
                string parameters = args[0];
                if (parameters[0] != 'c' || parameters[1] != ':')
                {
                    YoureAFailureHarry();
                    return;
                }

                try
                {
                    queries = Convert.ToInt32(parameters.Substring(2));
                }
                catch (Exception ex)
                {
                    YoureAFailureHarry();
                    return;
                }
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("----==== ABOUT TO GO BING MYSELF ====----");

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Loading dictionary binary into memory...");
            WordDictionary wordDictionary = new WordDictionary();
            int nWords = wordDictionary.Words.Length;

            Console.WriteLine("Let's get random!");
            Random randomWordCount = new Random();
            Random randomWordsUsed = new Random();

            List<String> phraseList = new List<String>();
            String phrase = String.Empty;
            for (int i = 0; i < queries; i++)
            {
                phrase = String.Empty;
                // Random words between 1 and 6 used in query.
                for (int j = 0; j < randomWordCount.Next(1, 6); j++)
                    phrase += wordDictionary.Words[randomWordsUsed.Next(nWords)] + " ";

                phraseList.Add(phrase);
            }

            Console.WriteLine("Let's see what crazy words we came up with...");
            Console.WriteLine("========================================================");
            Console.ResetColor();

            foreach(String savedPhrase in phraseList)
                Console.WriteLine(savedPhrase);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("========================================================");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Press any key to go Bing yourself...");
            Console.ReadKey();

            LaunchQueries(phraseList);
            
        }

        private static void YoureAFailureHarry()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Usage: c:#");
            Console.WriteLine("# = number of random queries");
            Console.ReadKey();
            return;
        }

        private static void LaunchQueries(List<String> phraseList)
        {
            StringBuilder sb = new StringBuilder();
            StringParser sp = new StringParser();
            foreach (String savedPhrase in phraseList)
            {
                List<String> words = sp.Parse(savedPhrase);

                sb.Clear();
                sb.Append("http://www.bing.com/search?setmkt=en-US&q=");
                
                foreach(String word in words)
                    sb.Append(word).Append("+");

                System.Diagnostics.Process.Start(sb.ToString());
            }
        }
    }
}
